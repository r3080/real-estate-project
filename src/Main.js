import React, {useState} from 'react'
import './Main.css';
import Homepage from './components/homepage/homepage';
import Login from './components/login/login';
import Register from './components/register/register';
import {Route, Routes} from "react-router-dom"
import {useNavigate} from "react-router-dom"
import App from './App';
import Houses from './pages/Houses';
import Home from './pages/Home';

function Main() {

  const Navigate = useNavigate();
  const [user, setLoginUser] = useState({})

    return (
    <div className="Main">
      <Routes>
        <Route exact path="/" element={<Homepage/>} />
        <Route   path="/login" element={<Login/>} />
        <Route  path="/register" element={<Register/>} />
        <Route  path="/app" element={<App/>} /> 
        <Route path="/home" element={<Houses/>}/>
        <Route path="/houses" element={<Houses/>}></Route>
        </Routes>
        
         </div>
  );
}

export default Main;

