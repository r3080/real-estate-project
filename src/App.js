import React from "react";
import './App.css';
import Home from './pages/Home';
import Properties from './pages/Properties';
import  Error from './pages/Error';
import Navbar from './components/Navbar';

import {BrowserRouter as Router, Route, Routes, Switch} from 'react-router-dom';

function App() {
  return <>
    <Navbar/>
    <Routes>
     
      <Route exact path='/' element={<Home />}/>
      <Route  path='/properties' element={<Properties />}/>
      <Route path='*' element={<Error/>}/>
   
    </Routes>
 
   
  </>;
}

export default App;
