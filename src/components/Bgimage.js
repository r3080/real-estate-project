import React from 'react'

export default function Bgimage({children,bgimage}) {
  return <header className={bgimage}>{children}</header>
}
Bgimage.defaultProps = {
    bgimage: "defaultBgimage"
  };