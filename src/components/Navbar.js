import React, { Component } from 'react'
import logo from '../images/logo.jpg';
import {FaAlignRight} from 'react-icons/fa';
import{Link} from 'react-router-dom';


export default class Navbar extends Component {
    state={
        isOpen:false
    }
    handleToggle = () =>{
        this.setState({isOpen:!this.state.isOpen})
    }
  render() {
    return (
      <nav className="navbar">
          <div className="nav-center">
          <div className="nav-header">
              <div className="nav-image">
          <Link to="/">
              <img className='photo' src={logo}  alt="Real Estate"/>
          </Link>
            </div>
          <button type="button" 
          className="nav-btn"
          onClick={this.handleToggle}
          >
          <FaAlignRight className="nav-icon"/>
          </button>
          </div>
            <ul className={this.state.isOpen?"nav-links show-nav":"nav-links"}>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/properties">Properties</Link>
                </li>
            </ul>
          </div>
      </nav>

    )
  }
}
