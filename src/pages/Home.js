import React from 'react';
import Bgimage from "../components/Bgimage";
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import Services from "../components/Services";

export default function Home() {
  return (
  <React.Fragment>
   <Bgimage>
    <Banner title="luxurious rooms" subtitle="deluxe rooms starting at $2000">
      <Link to='/properties' className="btn-primary">
        our houses
      </Link>
    </Banner>
  </Bgimage>  
  <Services/>
  </React.Fragment>
  )
}

