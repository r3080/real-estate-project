import React from 'react'
import Bgimage from '../components/Bgimage';
import Banner from '../components/Banner';
import {Link} from "react-router-dom";

export default function Properties() {
  return <Bgimage bgimage="propertiesBgimage" >
    <Banner title="houses">
      <Link to="/" className='btn-primary'>
        Return home
      </Link>
    </Banner>
  </Bgimage>
  
}



