import React from 'react'
import Bgimage from '../components/Bgimage';
import Banner from "../components/Banner";
import {Link} from 'react-router-dom';

export default function Error() {
  return <Bgimage>
    <Banner title='404' subtitle="page not found">
      <Link to = '/' className='btn-primary'>
        return home
      </Link>
    </Banner>
  </Bgimage>
}
